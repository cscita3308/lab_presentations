---?color=linear-gradient(to right, #c02425, #f0cb35)
@title[Introduction]

@snap[west text-25 text-bold text-white]
Lab 2<br>Working with Bash
@snapend


---?image=resources/img/bg/orange.jpg

@snap[north-west span-40]
@box[bg-green text-white demo-box-step-padding rounded](1. Create a basic Bash script)
@snapend

@snap[north]
@fa[arrow-right white fa-3x]
@snapend

@snap[north-east span-40]
@box[bg-purple text-white demo-box-step-padding rounded](2. Backup your code to Github)
@snapend

@snap[east]
@fa[arrow-down white fa-3x]
@snapend

@snap[south-east span-40]
@box[bg-pink text-white demo-box-step-padding rounded](3. Create a Bash script to handle Regex)
@snapend

@snap[south]
@fa[arrow-left white fa-3x]
@snapend

@snap[south-west span-40]
@box[bg-blue text-white demo-box-step-padding rounded](4. Backup to Github & submit a link to Moodle)
@snapend

@snap[midpoint span-20]
@box[bg-white text-black rounded](Lab Steps)
@snapend



---?image=resources/img/bg/orange.jpg&position=right&size=60% 100%
@title[Bash Scripting]

@snap[west text-16 text-bold text-italic text-orange span-30]
![Terminal Icon](resources/img/terminal.png)
@snapend

@snap[east text-white span-55]
@ul[split-screen-list text-07](false)
- Bash (Bourne again shell) is a popular scripting language for Linux.
- Has the ability to run other commands via the terminal (like grep or installed executables)
- A popular choice for handling system configurations when installing software or other tedious systems tasks
- Can be used to run programmatic code (just like Java/Python) but it's true strength comes from running terminal commands
@ulend
@snapend


<h4 style="position:fixed;  color:white; float:right;margin-left:97%;margin-top:28%;">Tutorials&darr;</h4>


+++?color=white

@snap[north span-60]
@box[bg-orange text-white](Cheat Sheet - Bash Commands #[Quick Handout](https://devhints.io/bash))
@snapend


@snap[midpoint span-60]
@box[bg-orange text-white](Online Tutorial #[Bash Scripting TutorialsPoint](https://www.tutorialspoint.com/unix/shell_scripting.htm))
@snapend


@snap[south span-60]
@box[bg-orange text-white](Books Covering Bash Scripting #[Bash Pocket Reference](https://bit.ly/2WtJCKz) <br> [Shell Scripting Bible](https://bit.ly/2B62ypx))
@snapend


---?color=white
@title[Sample Script]

```bash
#!/bin/bash
# Author : Your Name
# Date: 1/1/2019
# Script follows here:
echo "Enter a number: "
read numOne
echo "Enter a second number: "
read numTwo
sum=$(($numOne + $numTwo))
echo "The sum is : $sum"
let prod=numOne*numTwo
echo "The product is: $prod"
echo "File Name: $0"
echo "Command Line Argument 1: $1"
```

@[1](Define which type of scripting language you are using)
@[5,7,10,12](Echo prints messages to the terminal)
@[6,8](read will store user input into a variable)
@[9](To handle math expressions $(( )) use double paranthesis)
@[11](The Let command is another option for arithmetic in Bash, no spacing is allowed!)
@[13-14](To handle command line arguments use $number.  $0 - file name, $1 - first user argument)

@snap[east span-45]
@box[bg-orange text-white](Practice Script #Use the &larr; and &rarr; to cycle through the example code)
@snapend
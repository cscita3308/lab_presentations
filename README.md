## Creating Presentations

To create a new lab you'll need to:
1. Create a new lab folder.  (lab#_short_title)
2. Add a PITCHME.MD & PITCHME.yaml file to your folder (Copy one of the existing lab yaml files)
3. Modify the PITCHME.MD (following gitpitch's documentation) to create your presentation slides.
4. Place any images inside the resources/img directory

## Gitpitch Source Documents

* https://gitpitch.com/docs

* https://github.com/gitpitch/gitpitch

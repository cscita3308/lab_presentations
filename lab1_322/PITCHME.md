---?color=linear-gradient(to right, #c02425, #f0cb35)
@title[Introduction]

@snap[west text-25 text-bold text-white]
Lab 1<br>*Initial System Setup*
@snapend


---?image=resources/img/bg/orange.jpg&position=right&size=50% 100%
@title[Lab Format]

@snap[west text-16 text-bold text-italic text-orange span-40]
Format for Course Labs
@snapend

@snap[east text-white span-45]
@ul[split-screen-list text-08](false)
- Instructions - <br>HTML file posted to Moodle
- Presentation - <br>Gitpitch link posted to Moodle
- Submissions  - <br>HTML link to your github repo, submitted to Moodle
- Attendance   - <br>@size[2em](20%) of lab grade 
@ulend
@snapend

---?image=resources/img/bg/orange.jpg&position=right&size=50% 100%
@title[Linux OS]

@snap[west text-16 text-bold text-italic text-orange span-40]
![Ubuntu Logo](resources/img/ubuntu-logo.png)
@snapend

@snap[east text-white span-45]
@ul[split-screen-list text-08](false)
- Open Source Operating System
- Popular with programmers, especially when working with open source software!
@ulend
@snapend


<h4 style="position:fixed;  color:white; float:right;margin-left:97%;margin-top:28%;">Tutorials&darr;</h4>


+++?color=white

@snap[north span-60]
@box[bg-orange text-white](Cheat Sheet - Terminal Commands #[PDF Handout](https://files.fosswire.com/2007/08/fwunixref.pdf))
@snapend


@snap[midpoint span-60]
@box[bg-orange text-white](Online Tutorials # [Unix & Linux](https://www.tutorialspoint.com/unix/) <br> [Ubuntu](https://www.tutorialspoint.com/ubuntu/index.htm))
@snapend


@snap[south span-60]
@box[bg-orange text-white](Book Covering Linux Skills #[Library Access - The Linux Command Line](https://bit.ly/2TggNzk))
@snapend


---?image=resources/img/bg/orange.jpg&position=right&size=50% 100%
@title[VirtualBox]

@snap[west text-16 text-bold text-italic text-orange span-40]
![VirtualBox Logo](resources/img/Vbox_logo.png)
@snapend

@snap[east text-white span-45]
@ul[split-screen-list text-08](false)
- Hypervisor to manage a virtual machine
- OPTIONAL! Only needed if you don't want to install Linux on your machine.
- VMWARE and/or the departments VM are also usable for the course labs.
@ulend
@snapend

<h4 style="position:fixed;  color:white; float:right;margin-left:97%;margin-top:28%;">Tutorials&darr;</h4>

+++?color=white

@snap[north span-60]
@box[bg-orange text-white](VirtualBox Manual #[Manual Here](https://www.virtualbox.org/manual/ch01.html))
@snapend


@snap[midpoint span-60]
@box[bg-orange text-white](Online Tutorials # [Virtualization](https://www.tutorialspoint.com/virtualization2.0/index.htm))
@snapend


@snap[south span-60]
@box[bg-orange text-white](Book Covering Virtualization  #[Virtualization Essentials](https://bit.ly/2S81L1c))
@snapend


---?image=resources/img/bg/orange.jpg&position=right&size=50% 100%
@title[Git]

@snap[west text-16 text-bold text-italic text-orange span-40]
![Git Logo](resources/img/git_logo.png)
@snapend

@snap[east text-white span-45]
@ul[split-screen-list text-08](false)
- Version control software
- Manages a shared code repository
- Track changes in the code over time
- Handle merging of code from multiple developers
- Enable comments to track why/when changes were made
@ulend
@snapend

<h4 style="position:fixed;  color:white; float:right;margin-left:97%;margin-top:28%;">Tutorials&darr;</h4>

+++?color=white

@snap[north span-60]
@box[bg-orange text-white](Git Cheat Sheet #[Manual Here](https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf))
@snapend


@snap[midpoint span-60]
@box[bg-orange text-white](Online Tutorials # [Git Tutorial](https://www.tutorialspoint.com/git/))
@snapend


@snap[south span-60]
@box[bg-orange text-white](Git Books  #[Pro Git](https://bit.ly/2UjdY0j) <br> [Git for Team](https://bit.ly/2RfHQZQ))
@snapend




---?image=resources/img/bg/orange.jpg&position=right&size=50% 100%
@title[GitHub]

@snap[west text-16 text-bold text-italic text-orange span-40]
![Github Logo](resources/img/github_logo.png)
@snapend

@snap[east text-white span-45]
@ul[split-screen-list text-08](false)
- Hosts & manages your code repository using Git
- Most popular online repo
- Notable competitors - GitLab & BitBucket
@ulend
@snapend

---?image=resources/img/bg/orange.jpg&position=right&size=50% 100%
@title[PostgreSQL]

@snap[west text-16 text-bold text-italic text-orange span-40]
![PostgreSQL Logo](resources/img/postgresql_logo.png)
@snapend

@snap[east text-white span-45]
@ul[split-screen-list text-08](false)
- A version of SQL (Relational Database)
- Lightweight & supported by Heroku (tool used later on for hosting our web services)
@ulend
@snapend

<h4 style="position:fixed;  color:white; float:right;margin-left:97%;margin-top:28%;">Tutorials&darr;</h4>

+++?color=white

@snap[north span-60]
@box[bg-orange text-white](PostgreSQL Cheat Sheet #[Cheat Sheet](http://www.postgresqltutorial.com/wp-content/uploads/2018/03/PostgreSQL-Cheat-Sheet.pdf))
@snapend


@snap[midpoint span-60]
@box[bg-orange text-white](Online Tutorials # [PostgreSQL Tutorial](https://www.tutorialspoint.com/postgresql/))
@snapend


@snap[south span-60]
@box[bg-orange text-white](PostgreSQL Book  #[Learning PostgreSQL](https://bit.ly/2S7sYAZ))
@snapend

---?image=resources/img/bg/orange.jpg&position=right&size=50% 100%
@title[Node.js]

@snap[west text-16 text-bold text-italic text-orange span-40]
![Node.js Logo](resources/img/nodejs_logo.png)
@snapend

@snap[east text-white span-45]
@ul[split-screen-list text-08](false)
- A framework to allow for Server-Side programming in Javascript
- Allows for the entire web service (Client/Server) to be written in one language (Javascript)
@ulend
@snapend

<h4 style="position:fixed;  color:white; float:right;margin-left:97%;margin-top:28%;">Tutorials&darr;</h4>

+++?color=white

@snap[north span-60]
@box[bg-orange text-white](Javascript Cheat Sheet #[Cheat Sheet](https://htmlcheatsheet.com/js/))
@snapend


@snap[midpoint span-60]
@box[bg-orange text-white](Online Tutorials # [Javascript Tutorial](https://www.tutorialspoint.com/javascript/index.htm))
@snapend


@snap[south span-60]
@box[bg-orange text-white](Javascript Book  # [JavaScript and JQuery: Interactive Front-End Web Development](https://bit.ly/2UjFYBe))
@snapend